#ifndef FEM_HPP
#define FEM_HPP

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif
#pragma warning (disable : 4244 4018)

#include <fem/do.hpp>
#include <fem/data_of_type.hpp>
#include <fem/error_utils.hpp>
#include <fem/format.hpp>
#include <fem/intrinsics.hpp>
#include <fem/main.hpp>
#include <fem/major_types.hpp>

#endif // GUARD
